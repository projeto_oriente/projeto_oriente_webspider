import threading
from queue import Queue
from urllib.parse import urljoin

import requests
from bs4 import BeautifulSoup
from peewee import DoesNotExist

from oriente_webspider.models import Documento, Arquivo, remove_url_protocol, add_url_scheme

"""
Modulo responsavel pela identificação de links de arquivos dos documentos
"""


def get_links_function(repo):
    def repo_links(links):
        repo_url = add_url_scheme(repo)
        links_response = []

        for l in links:
            if l['href'].startswith("/bitstream/"):
                url_arquivo = urljoin(repo_url, l['href'])
                links_response.append(url_arquivo)

        return links_response

    return repo_links


repo_extract_links = {
    "//acervodigital.ufpr.br": get_links_function("//acervodigital.ufpr.br"),
    "//repositorio.bc.ufg.br": get_links_function("//repositorio.bc.ufg.br"),
    "//repositorio.uft.edu.br": get_links_function("//repositorio.uft.edu.br"),
}


def get_links(html, repo):
    html_soupe = BeautifulSoup(html, features="lxml")
    links = html_soupe.find_all("a", href=True)
    return repo_extract_links[repo](links)


def get_info_arquivo_worker(documento: Documento):
    print(documento)
    url = add_url_scheme(documento.url)
    response = requests.get(url)
    links = get_links(response.text, documento.repositorio.url)
    for link in links:
        arquivo_url = remove_url_protocol(link)
        try:
            arquivo = Arquivo.get(Arquivo.url == arquivo_url)
        except DoesNotExist:
            arquivo = Arquivo(
                documento=documento,
                url=arquivo_url,
            )
            arquivo.save()


def get_info_arquivo_thread(index, q):
    print("init-> get_info_arquivo_thread:", index)
    while True:
        documento = q.get()
        get_info_arquivo_worker(documento)
        q.task_done()


def run():
    q = Queue()
    max_threads = 10
    threads = []

    # cria threads
    for i in range(max_threads):
        t = threading.Thread(
            target=get_info_arquivo_thread,
            args=(
                i,
                q,
            ),
        )
        t.setDaemon(True)
        t.start()
        threads.append(t)

    documentos = Documento.select()
    for documento in documentos:
        q.put(documento)

    q.join()


if __name__ == "__main__":
    run()
