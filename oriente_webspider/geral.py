import threading
from queue import Queue

import requests
import validators
from models import Repositorio, Documento
from sickle import Sickle

repositorios = list()


class RepositorioOAI(object):
    url = ""
    endpoint = ""
    metadata_prefix = "oai_dc"

    def is_handle(self, url):
        return validators.url(url)


class RepositorioUFT(RepositorioOAI):
    url = "http://repositorio.uft.edu.br"
    endpoint = "http://repositorio.uft.edu.br/oai/request"


repositorios.append(RepositorioUFT)


class RepositorioUFPR(RepositorioOAI):
    url = "http://acervodigital.ufpr.br"
    endpoint = "http://acervodigital.ufpr.br/oai/request"


repositorios.append(RepositorioUFPR)


class RepositorioUFG(RepositorioOAI):
    url = "http://repositorio.bc.ufg.br"
    endpoint = "http://repositorio.bc.ufg.br/oai/request"


repositorios.append(RepositorioUFG)
"""
class RepositorioUFPA(RepositorioOAI):
    url = "http://www.repositorio.ufpa.br"
    endpoint = "http://www.repositorio.ufpa.br/oai/request"
repositorios.append(RepositorioUFPA)

class RepositorioUFMA(RepositorioOAI):
    url = "http://www.repositorio.ufma.br:8080"
    endpoint = "http://www.repositorio.ufma.br:8080/oai/request"
repositorios.append(RepositorioUFMA)
"""


def processa_record_worker(record, repo, repo_instance):
    identifier = None

    try:

        for idf in record.metadata["identifier"]:
            if repo.is_handle(idf):
                identifier = idf

        if identifier is None:
            return

        response = requests.get(identifier)
        url_documento = response.url

        if response.status_code == 404:
            return

        titulo_documento = ""
        for titulo in record.metadata["title"]:
            if len(titulo) > 0:
                titulo_documento = titulo
                break

        if not len(titulo_documento) > 0:
            titulo_documento = "{}".format(url_documento)

        documento, documento_criado = Documento.get_or_create(
            repositorio=repo_instance,
            url=url_documento,
            titulo=titulo_documento,
        )



    except AttributeError as e:
        print(e)
    except KeyError as e:
        print(e)


def worker_record_thread(parent_index, index, q):
    while True:
        print("processa_repositorio_worker: {} worker_record_thread: {}".format(parent_index, index))
        record, repo, repo_instance = q.get()
        processa_record_worker(record, repo, repo_instance)
        q.task_done()


def processa_repositorio_worker(index, q):
    documento_queue = Queue()
    documento_queue_max = 6
    threads = []

    print("inicio processa_repositorio_worker: ", index)

    while True:
        repo = q.get()
        print("Inicio: ", repo.url)
        repositorio, repositorio_criado = Repositorio.get_or_create(url=repo.url)
        len_record = 0
        sickle = Sickle(repo.endpoint)
        records = sickle.ListRecords(metadataPrefix=repo.metadata_prefix)

        for i in range(documento_queue_max):
            t = threading.Thread(
                target=worker_record_thread,
                args=(
                    index,
                    i,
                    documento_queue,
                )
            )
            threads.append(t)
            t.start()

        record = records.next()
        while record is not None:
            documento_queue.put((record, repo, repositorio))
            len_record += 1
            try:
                record = records.next()
            except Exception as e:
                record = None
                print(e)
        print("Fim: ", repo.url, len_record)

        q.task_done()

    print("fim processa_repositorio_worker: ", index)


def run(repositorios, repositorios_queue_max=2):
    print("RUN REPOSITORIOS")
    repositorios_queue = Queue()
    threads = []

    for i in range(repositorios_queue_max):
        t = threading.Thread(
            target=processa_repositorio_worker,
            args=(
                i,
                repositorios_queue,
            )
        )
        threads.append(t)
        t.start()

    for repositorio in repositorios:
        repo_objeto = repositorio()
        repositorios_queue.put(repo_objeto)


if __name__ == "__main__":
    run(repositorios)
