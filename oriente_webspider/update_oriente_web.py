from queue import Queue
from threading import Thread

from decouple import config
from oriente_libapi import Documento, ApiManager

from oriente_webspider.models import Documento as DBDocumento

api_manager = ApiManager(
    user=config("oriente_user", default="admin"),
    password=config("oriente_password", default="1234567o"),
    endpoint=config("oriente_endpoint", default="https://oriente.pythonanywhere.com/api"),
)

Documento.api_manager = api_manager


def update_oriente_web_worker(index, q: Queue):
    while True:
        documento = q.get()
        documento_dict = documento.to_dict()
        doc_dict = {
            "url": documento_dict.get("url"),
            "url_arquivo": documento_dict.get("arquivo"),
            "titulo": documento_dict.get("titulo"),
        }
        doc = Documento(data=doc_dict)
        try:
            doc.save()
        except Exception as e:
            print(e)


def update_oriente_web(max_threads=12):
    q = Queue()

    for i in range(max_threads):
        t = Thread(
            target=update_oriente_web_worker,
            args=(i, q),
        )
        t.setDaemon(True)
        t.start()

    for documento in DBDocumento.select():
        q.put(documento)

    q.join()


if __name__ == '__main__':
    update_oriente_web()
