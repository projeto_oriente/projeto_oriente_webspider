from queue import Queue
from threading import Thread

from oriente_webspider.models import (
    Repositorio,
    Documento,
    Arquivo,
    remove_url_protocol,
    add_url_scheme,
)

"""
Antes de executar este arquivo, deve executar o get_url_arquivos.py
"""


def clean_urls_repositorios():
    repositorios = Repositorio.select()
    for repositorio in repositorios:
        repositorio.url = remove_url_protocol(repositorio.url)
        repositorio.save()


def clean_urls_documentos():
    documentos = Documento.select()
    for documento in documentos:
        documento.url = remove_url_protocol(documento.url)
        documento.save()


def tipo_arquivo_aceito(url, mmtype_aceito):
    import mimetypes
    import urllib.request

    mmtype = []

    try:
        with urllib.request.urlopen(url) as response:
            info = response.info()
            mmtype.append(info.get_content_type())
    except urllib.error.HTTPError:
        return False

    mimetypes.init()
    mmresponse = mimetypes.guess_type(url, strict=False)

    if type(mmresponse[0]) == str:
        mmtype.append(mmresponse[0])

    for mmt in mmtype:
        if mmt in mmtype_aceito:
            return True
    return False


def clean_arquivos_worker(index, q: Queue):
    while True:
        arquivo = q.get()
        if not tipo_arquivo_aceito(add_url_scheme(arquivo.url), ["application/pdf", ]):
            print(f"deleting {arquivo}")
            arquivo.delete_instance()
        q.task_done()


def clean_arquivos(max_threads=12):
    q = Queue()

    for i in range(max_threads):
        t = Thread(
            target=clean_arquivos_worker,
            args=(i, q),
        )
        t.setDaemon(True)
        t.start()

    for arquivo in Arquivo.select():
        q.put(arquivo)
    q.join()


def remove_documentos_arquivos_demais(documento: Documento):
    if documento.arquivos.count() > 1:
        print(f"deleting {documento} com arquivos demais")
        documento.delete_instance(recursive=True)
    else:
        return documento


def remove_documentos_sem_arquivo(documento: Documento):
    if documento.arquivos.count() == 0:
        print(f"deleting {documento} com arquivos de menos")
        documento.delete_instance()
    else:
        return documento


def verifica_arquivo_no_repositorio_correto(documento: Documento):
    for arquivo in documento.arquivos:
        if documento.repositorio.url not in arquivo.url:
            print(f"d: {documento.repositorio.url} a:{arquivo.url}")
            # arquivo.delete_instance()

    return documento


def clean_documentos_worker(index, q: Queue):
    while True:
        documento = q.get()
        verifica_arquivo_no_repositorio_correto(documento)
        documento = remove_documentos_sem_arquivo(documento)
        if documento is not None:
            documento = remove_documentos_arquivos_demais(documento)
        q.task_done()


def clean_documentos():
    q = Queue()
    max_threads = 10

    for i in range(max_threads):
        t = Thread(
            target=clean_documentos_worker,
            args=(i, q),
        )
        t.setDaemon(True)
        t.start()

    for index, documento in enumerate(Documento.select()):
        q.put(documento)

    q.join()


def main():
    # clean_urls_repositorios()
    # clean_arquivos()
    clean_documentos()


if __name__ == '__main__':
    main()
