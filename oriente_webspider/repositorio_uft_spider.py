import re

import scrapy
from decouple import config
from models import Documento as LocalDocumento
from models import Repositorio as LocalRepositorio
from models import remove_url_protocol
from oriente_libapi import Documento, ApiManager
from peewee import DoesNotExist

api_manager = ApiManager(
    user=config("oriente_user", default="admin"),
    password=config("oriente_password", default="1234567o"),
    endpoint=config("oriente_endpoint", default="http://127.0.0.1:8000/api"),
)

Documento.api_manager = api_manager

repositorio_local_db, repositorio_local_db_criado = LocalRepositorio.get_or_create(url="//repositorio.uft.edu.br")


def is_handle(url):
    if re.match(r"https://repositorio.uft.edu.br/handle/\d+/\d+$", url) is not None:
        return True
    elif re.match(r"https://repositorio.uft.edu.br/handle/\d+/\d+/$", url) is not None:
        return True
    return False


def is_proximo(texto, a):
    if re.match(a, texto) is not None:
        return True
    return False


class RIUFTSpider(scrapy.Spider):
    name = "Repositorio UFT Spider"
    allowed_domains = ["repositorio.uft.edu.br"]
    start_urls = ["https://repositorio.uft.edu.br/browse?type=title"]

    def parse(self, response):
        # verificar se url é handle
        handle_test = False
        url = response.url
        if re.match(r"https://repositorio.uft.edu.br/handle/\d+/\d+$", url):
            handle_test = True
            url = "{}/".format(url)
        elif re.match(r"https://repositorio.uft.edu.br/handle/\d+/\d+/$", url):
            handle_test = True

        links = response.css("a::attr(href)").extract()

        if handle_test:
            yield scrapy.Request(url, callback=self.parse_handle)

        for a in response.css("a"):
            texto = a.css("a::text").extract_first()
            if texto is None:
                texto = "textovazio"
            href = a.css("a::attr(href)").extract_first()

            ja = response.urljoin(href)

            if is_handle(ja):
                yield scrapy.Request(ja, callback=self.parse_handle)
            elif is_proximo(texto, r"next"):
                yield scrapy.Request(ja, callback=self.parse)

    def parse_handle(self, response):
        url = response.url
        has_files_test = False
        url_arquivo = None
        titulo = ""

        links = response.css("a::attr(href)").extract()

        for a in links:
            if re.match(r"/bitstream/\d+/\d+", a) is not None:
                has_files_test = True
                url_arquivo = response.urljoin(a)
                break

        titulos = response.css("td.dc_title").css(".metadataFieldValue::text").extract()

        for t in titulos:
            titulo = t

        print("*" * 40)
        print("*" * 40)
        print("*" * 40)
        url = remove_url_protocol(url)
        print(url)
        print(titulo)
        print(repositorio_local_db)

        try:
            doc = LocalDocumento.get(LocalDocumento.url == url)
        except DoesNotExist:
            doc = LocalDocumento(
                repositorio=repositorio_local_db,
                titulo=titulo,
                url=url,
            )
            doc.save()

        """
        if has_files_test and url_arquivo is not None:
            doc = Documento(data = {
                "url": url,
                "url_arquivo": url_arquivo,
                "titulo": titulo,
            })
            doc.save()
        """
