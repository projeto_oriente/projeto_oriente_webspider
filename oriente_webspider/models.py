from urllib import parse

from peewee import DoesNotExist
from peewee import (
    Model,
    CharField,
    SqliteDatabase,
    ForeignKeyField,
)

db = SqliteDatabase("dataset.db")


def remove_url_protocol(url):
    a = parse.urlparse(url)
    b = parse.ParseResult('', *a[1:])
    return b.geturl()


def add_url_scheme(url):
    return "http:{}".format(url)


class Repositorio(Model):
    url = CharField(unique=True)

    class Meta:
        database = db

    def __str__(self):
        return "{}: {}".format(self.id, self.url)


class Documento(Model):
    repositorio = ForeignKeyField(Repositorio, backref="documentos")
    titulo = CharField()
    url = CharField(unique=True)

    class Meta:
        database = db

    def get_arquivo(self):
        """
        Seleciona somente um arquivo, no caso de mais de um arquivo o primeiro retornado é usado
        :return: string
        """
        for arquivo in self.arquivos:
            return arquivo

    def to_dict(self):
        return {
            "id": self.id,
            "repositorio": add_url_scheme(self.repositorio.url),
            "titulo": self.titulo,
            "url": add_url_scheme(self.url),
            "arquivo": add_url_scheme(self.get_arquivo().url)
        }

    def safe_create(repositorio, titulo, url):
        try:
            doc = Documento.get(Documento.url == url)
            doc.titulo = titulo
            doc.repositorio = repositorio
            doc.save()
        except DoesNotExist as e:
            doc = Documento(
                url=url,
                titulo=titulo,
                repositorio=repositorio
            )
            doc.save()

        return doc

    def __str__(self):
        return "{}: {}".format(self.id, self.url)


class Arquivo(Model):
    documento = ForeignKeyField(Documento, backref="arquivos")
    url = CharField(unique=True)

    class Meta:
        database = db

    def __str__(self):
        return "{}: {}".format(self.id, self.url)


def init():
    db.connect()
    db.create_tables([Repositorio, Documento, Arquivo])
    db.close()

if __name__ == "__main__":
    init()
