from sickle import Sickle

endpoints = [
    "http://repositorio.uft.edu.br/oai/request",
    "http://acervodigital.ufpr.br/oai/request",
    "http://repositorio.bc.ufg.br/oai/request",
    "http://www.repositorio.ufpa.br/oai/request",
    "http://www.repositorio.ufma.br:8080/oai/request",
]

len_records = 0

for endpoint in endpoints:
    print("")
    print(endpoint, "*" * 20)

    sickle = Sickle(endpoint)
    records = sickle.ListRecords(metadataPrefix='oai_dc')

    for record in records:
        for meta in record.metadata:
            print(meta, ": ", record.metadata[meta])
        break
