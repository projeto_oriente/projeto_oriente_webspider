from scrapy.crawler import CrawlerProcess

from .repositorio_uft_spider import RIUFTSpider

name = "oriente_webspider"

process = CrawlerProcess({
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
})

if __name__ == "__main__":
    process.crawl(RIUFTSpider)
    process.start()
