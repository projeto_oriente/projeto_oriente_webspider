import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

install_dependencies = [
    "Sickle==0.6.4",
    "Scrapy==1.5.1",
    "python-decouple==3.1",
    "requests==2.21.0",
    "validators==0.12.4",
]

setuptools.setup(
    name="oriente_webspider",
    version="0.0.1",
    author="Paulo Roberto Cruz",
    author_email="paulo.cruz9@gmail.com",
    description="webspider para pegar registros de documentos para o oriente_web",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="http://gitlab.com/grupo_oriente/projeto_oriente_webspider/",
    packages=setuptools.find_packages(),
    install_requires=install_dependencies,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Webscraping",
    ]
)
